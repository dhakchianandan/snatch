import React, { useEffect, useState } from 'react';

import Notification from './Notification';
import Snatches from './Snatches';

function Downloads() {
  const [files, setFiles] = useState(() => {
    return JSON.parse(localStorage.getItem('files') || JSON.stringify([]));
  });

  useEffect(() => {
    localStorage.setItem('files', JSON.stringify(files));
  }, [files]);

  return (
    <section className='w-full md:w-2/3 lg:w-1/3'>
      {files.length === 0 && <Notification text='No Snatches' />}

      {files.length > 0 &&
        files.map((file, index) => (
          <Snatches file={file} key={`${file.id}-${index}`} />
        ))}

      {files.length > 0 && (
        <button
          className='bg-gray-900 w-full py-2 rounded-lg text-white font-bold font-serif focus:outline-none'
          onClick={() => setFiles([])}
        >
          Clear snatches
        </button>
      )}
    </section>
  );
}

export default Downloads;
