import React from 'react';
import filesize from 'filesize';

function Snatches({ file }) {
  return (
    <li className='flex justify-between bg-gray-50 border-2 rounded-lg px-4 py-2 items-center mb-2'>
      <div className='w-2/3'>
        <p className='truncate text-md text-gray-600'>{file.name}</p>
        <p className='font-serif text-sm text-gray-400'>
          {filesize(file.size)}
        </p>
      </div>
      <div>
        <a href={file.url} download>
          <svg
            className='h-6'
            xmlns='http://www.w3.org/2000/svg'
            fill='none'
            viewBox='0 0 24 24'
            stroke='currentColor'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth={2}
              d='M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M9 19l3 3m0 0l3-3m-3 3V10'
            />
          </svg>
        </a>
      </div>
    </li>
  );
}

export default Snatches;
