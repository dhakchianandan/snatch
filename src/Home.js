import React, { useEffect, useState } from 'react';
import axios from 'axios';

import Notification from './Notification';
import Snatches from './Snatches';

function Home() {
  const [url, setUrl] = useState(
    // 'magnet:?xt=urn:btih:eaa2fcee9910f2bb981afaeba593fb4b4593b202&dn=www.1TamilMV.us%20-%20Thanneer%20Mathan%20Dinangal%20(2019)%20Malayalam%20TRUE%20HD%20-%201080p%20-%20AVC%20-%20AAC%20-%203.4GB%20-%20ESub.mkv&tr=udp%3a%2f%2fp4p.arenabg.com%3a1337%2fannounce&tr=http%3a%2f%2fpow7.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.tiny-vps.com%3a6969%2fannounce&tr=http%3a%2f%2ftracker2.itzmx.com%3a6961%2fannounce&tr=udp%3a%2f%2f151.80.120.114%3a2710%2fannounce&tr=udp%3a%2f%2f9.rarbg.com%3a2790%2fannounce&tr=udp%3a%2f%2f9.rarbg.to%3a2740%2fannounce&tr=udp%3a%2f%2fopen.stealth.si%3a80%2fannounce&tr=udp%3a%2f%2ftracker.leechers-paradise.org%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce&tr=http%3a%2f%2ft.nyaatracker.com%3a80%2fannounce',
    '',
  );
  const [file, setFile] = useState(undefined);
  const [fetching, setFetching] = useState(false);
  const [errorMessage, setErrorMessage] = useState(undefined);

  useEffect(() => {
    if (file) {
      let files = JSON.parse(
        localStorage.getItem('files') || JSON.stringify([]),
      );
      files.unshift(file);
      localStorage.setItem('files', JSON.stringify(files.slice(0, 5)));
    }
  }, [file]);

  const fetchTorrent = async (event) => {
    event.preventDefault();
    setFile(undefined);
    setFetching(true);
    setErrorMessage(undefined);

    const { data } = await axios.get(process.env.REACT_APP_ALLDEBRID_ENDPOINT, {
      params: {
        agent: process.env.REACT_APP_ALLDEBRID_AGENT,
        apikey: process.env.REACT_APP_ALLDEBRID_TOKEN,
        link: url,
      },
    });

    const { status } = data;

    if (status === 'success') {
      const {
        data: { link },
      } = data;

      if (link) {
        const {
          data: { id, filename: name, link: url, filesize: size },
        } = data;

        if (size > 0) {
          setFile({ id, name, url, size });
        } else {
          setErrorMessage('Retry after sometime');
        }
      } else {
        setErrorMessage('Retry after sometime');
      }
    } else {
      const {
        error: { message },
      } = data;

      setErrorMessage(message);
    }

    setFetching(false);
    setUrl('');
  };

  return (
    <section className='w-full md:w-2/3 lg:w-1/3'>
      <form onSubmit={fetchTorrent} className='flex flex-col'>
        <textarea
          className='border-2 border-gray-300 rounded-lg text-center mb-1 focus:border-gray-400 focus:ring-0 font-serif'
          name='url'
          placeholder='enter url here'
          value={url}
          rows='3'
          onChange={({ target: { value } }) => setUrl(value)}
        ></textarea>

        <button
          type='submit'
          className={`bg-gray-900 ${
            fetching ? 'animate-pulse' : ''
          } py-2 rounded-lg text-white font-bold font-serif focus:outline-none mb-4`}
        >
          snatch
        </button>
      </form>

      {file && (
        <ul className='mt-8'>
          <Snatches file={file} />
        </ul>
      )}

      {errorMessage && <Notification text={errorMessage} type='bg-red-500' />}
    </section>
  );
}

export default Home;
