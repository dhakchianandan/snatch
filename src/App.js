import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink,
} from 'react-router-dom';

import Home from './Home';
import Downloads from './Downloads';

import './App.css';

import logo from './logo512.png';

function App() {
  return (
    <div className='container mx-auto flex flex-col items-center p-8'>
      <h1 className='text-3xl'>
        <img src={logo} alt='snatch' className='h-16 mb-8' />
      </h1>

      <Router>
        <nav className='flex border-2 rounded-full overflow-hidden mb-8'>
          <NavLink
            exact
            activeClassName='bg-gray-200 shadow-inner'
            className='px-6 py-2'
            to='/'
          >
            <svg
              className='h-4'
              xmlns='http://www.w3.org/2000/svg'
              viewBox='0 0 20 20'
              fill='currentColor'
            >
              <path d='M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z' />
            </svg>
          </NavLink>
          <NavLink
            exact
            activeClassName='bg-gray-200 shadow-inner'
            className='px-6 py-2'
            to='/downloads'
          >
            <svg
              className='h-4'
              xmlns='http://www.w3.org/2000/svg'
              fill='none'
              viewBox='0 0 24 24'
              stroke='currentColor'
            >
              <path
                strokeLinecap='round'
                strokeLinejoin='round'
                strokeWidth={2}
                d='M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4'
              />
            </svg>
          </NavLink>
        </nav>

        <Switch>
          <Route path='/downloads'>
            <Downloads />
          </Route>
          <Route path='/'>
            <Home />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
