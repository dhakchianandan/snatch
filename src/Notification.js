import React from 'react';

function Notification({ text, type = 'bg-green-500' }) {
  return (
    <div className='text-center'>
      <p
        className={`inline-block rounded-full  italic text-white font-light font-serif text-black px-4 py-1 ${type}`}
      >
        {text}
      </p>
    </div>
  );
}

export default Notification;
